var Git = require('nodegit');


class GitWrapper {
  constructor(site) {
    if (site) {
      this.ready = new Promise((resolve, reject) => {
        this.fetchOpts = {
          callbacks: {
            certificateCheck: () => 1,
            credentials: () => Git.Cred.userpassPlaintextNew(site.username, site.password)
          }
        }

        this.signature = Git.Signature.create('Connor Bode', 'connor.bode@gmail.com', (new Date).getTime() / 1000, 0)

        Git.Repository.open(site.repo).then((repo) => {
          this.repo = repo
          resolve()
        }).catch(reject)
      })
    } else {
      this.ready = Promise.resolve()
    }
  }

  clone (params) {
    return new Promise((resolve, reject) => {
      Git.Clone(params.uri, params.path, {
        fetchOpts: {
          callbacks: {
            certificateCheck: () => { return 1 },
            credentials: () => {
              return Git.Cred.userpassPlaintextNew(params.username, params.password)
            }
          }
        }
      }).then(resolve).catch((err) => {
        switch (err) {
          default:
            console.log(err)
            reject()
        }
      })
    })
  }

  isClean () {
    return new Promise((resolve, reject) => {
      this.ready
        .then(() => {
          return this.repo.getStatus()
        })
        .then((statuses) => {
          resolve(statuses.length === 0)
        })
        .then(() => {
        })
        .catch((err) => {
          console.log('err in catcher' + err)
        })
    })
  }

  pull (params) {
    return new Promise((resolve, reject) => {
      this.repo.refreshIndex()
        .then(() => {
          return this.repo.index()
        })
        .then((index) => {
          index.addByPath('.')
          index.write()
          return index.writeTree()
        })
        .then(() => {
          return this.repo.getMasterCommit()
        })
        .then((commit) => {
          return Git.Reset.reset(this.repo, commit, Git.Reset.TYPE.HARD)
        })
        .then(() => {
          return this.repo.fetch('origin', this.fetchOpts)
        })
        .then(() => {
          return this.repo.mergeBranches('master', 'origin/master')
        }).then(() => {
          resolve()
        }).catch(reject)
    })
  }

  push (params) {
    const signature = this.signature

    return new Promise((resolve, reject) => {
      this.repo.refreshIndex()
        .then(() => {
          return this.repo.index()
        })
        .then((index) => {
          index.addByPath('.')
          index.write()
          return Promise.all([
            index.writeTree(),
            this.repo.getHeadCommit(),
            this.repo.getStatus()
          ])
        })
        .then((data) => {
          const oid = data[0]
          const commit = data[1]
          const files = data[2].map((status) => status.path())
          return this.repo.createCommitOnHead(files, signature, signature, params.message)
        })
        .then((commit) => {
          return this.repo.getRemote('origin')
        })
        .then((remote) => {
          const refs = ["refs/heads/master:refs/heads/master"]
          return remote.push(refs, this.fetchOpts)
        })
        .then(() => {
          resolve()
        })
        .catch((err) => {
          console.log(err)
        })
    })
  }
}

module.exports = GitWrapper