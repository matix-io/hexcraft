const yaml = require('js-yaml')
const fs = require('fs')
const path = require('path')
const Git = require('./git')

class Site {
  constructor(config) {
    this.name = config.name
    this.repo = config.repo
    this.username = config.username
    this.password = config.password
    this.loadConfig()
  }

  loadConfigV1(config) {
    this.links = config.links.map((link) => {
      link.path = path.join(this.repo, 'source', link.path)
      return link
    })
  }

  loadConfig() {
    try {
      const configFile = fs.readFileSync(`${this.repo}/_hexcraft-config.yml`, 'utf8')
      const config = yaml.safeLoad(configFile)
      const version = config.version

      switch (version) {

      case 1:
        this.loadConfigV1(config)
        break

      default:
        throw "Unknown config file version."

      }
    } catch (err) {
      console.log(err)
    }
  }

  toJSON() {
    return {
      name: this.name,
      repo: this.repo,
      version: this.version,
      username: this.username,
      password: this.password
    }
  }
}


module.exports = Site