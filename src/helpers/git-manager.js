// This thing is just responsible for maintaining open Git repos
const Git = require('./git')

let gits = {}

module.exports = {
  get: function (site) {
    if (!gits[site.path]) {
      gits[site.path] = new Git(site)
    }
    return gits[site.path]
  }
}