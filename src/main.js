const { app, BrowserWindow, ipcMain } = require('electron')
const path = require('path')
const url = require('url')
const mkdirp = require('mkdirp')
const fs = require('fs')

const APP_NAME = 'developers-against-wordpress'
const HOME_OPTS = {
  WIN: process.env.USERPROFILE,
  OSX: process.env.HOME
}
const PLATFORM = process.platform === 'win32' ? 'WIN' : 'OSX'
const HOME = HOME_OPTS[PLATFORM]
const DATA_DIR = path.join(HOME, `.${APP_NAME}`)
const DATA_FILE = path.join(DATA_DIR, 'data.json')
const DATA_TEMPLATE = path.join(__dirname, 'data-template.json')

const CONFIG = {
  PLATFORM: PLATFORM,
  DATA_DIR: DATA_DIR,
  DATA_TEMPLATE: DATA_TEMPLATE,
  DATA_FILE: DATA_FILE,
  REPO_DIR: path.join(DATA_DIR, 'repos'),
}

const tasks = require('./tasks')(CONFIG)
const views = require('./views')(CONFIG, tasks)

let appData

app.on('ready', () => {
  appData = tasks.loadData()
  views.list(appData)
})

ipcMain.on('launch', (event, message) => {
  const View = views[message.view]
  View(message.args)
  event.returnValue = {}
})

ipcMain.on('close', (event, message) => {
  const View = views[message.view]
  View.close()
  event.returnValue = {}
})

ipcMain.on('edit', (event, message) => {
  let site
  let promise

  if (message.new) {
    promise = new Promise((resolve, reject) => {
      let site
      tasks.new(message.data)
        .then((_site) => {
          site = _site
          return tasks.install(site)
        })
        .then(() => {
          resolve(site)
        })
    })
  } else {
    promise = Promise.resolve()
  }

  promise.then((site) => {
    appData.sites.push(site)
    views.edit.close()
    views.list.update(appData)
    tasks.save(appData)
  })

  event.returnValue = {}
})

ipcMain.on('save', (event, message) => {
  tasks.push(message.site, message.message)
  event.returnValue = {}
})

ipcMain.on('discard', (event, message) => {
  tasks.sync(message.site).then(() => {})
  event.returnValue = {}
})