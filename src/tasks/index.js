module.exports = function (config) {
  return {
    new: require('./new')(config),
    save: require('./save')(config),
    sync: require('./sync')(config),
    launchServer: require('./launch-server')(config),
    loadData: require('./load-data')(config),
    push: require('./push')(config),
    isClean: require('./is-clean')(config),
    install: require('./install')(config),
  }
}