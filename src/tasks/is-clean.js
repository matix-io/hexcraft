const GitManager = require('../helpers/git-manager')


module.exports = function (config) {
  return function (site) {
    return new Promise((resolve, reject) => {
      var git = GitManager.get(site)
      git.isClean().then((isClean) => resolve(isClean))
    })
  }
}