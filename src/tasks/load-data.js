const mkdirp = require('mkdirp')
const Site = require('../helpers/site')
const fs = require('fs')


module.exports = function (config) {
  return function () {
    try {
      mkdirp.sync(config.DATA_DIR)
      mkdirp.sync(config.REPO_DIR)
    } catch (err) {
      console.log(err)
      process.exit(0)
    }

    try {
      raw = fs.readFileSync(config.DATA_FILE, 'utf8')
    } catch (err) {
      raw = fs.readFileSync(config.DATA_TEMPLATE)
      fs.writeFileSync(config.DATA_FILE, raw, 'utf8')
    }

    let data = JSON.parse(raw)
    data.sites = data.sites.map((site) => new Site(site))
    return data
  }
}