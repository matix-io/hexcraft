const GitManager = require('../helpers/git-manager')
const npm = require('npm-programmatic')
const fs = require('fs')


module.exports = function (config) {
  return function (site) {
    return new Promise((resolve, reject) => {
      const repo = GitManager.get(site)
      repo.ready.then(() => {
        return repo.pull()
      }).then(() => {
        let package = JSON.parse(fs.readFileSync(`${site.repo}/package.json`, 'utf8'))
        let name
        let version
        let packages = []
        for (name in package.dependencies) {
          try {
            let pkgPath = `${site.repo}/node_modules/${name}/package.json`
            let pkg = JSON.parse(fs.readFileSync(pkgPath, 'utf8'))
            // TODO check versioning
          } catch (err) {
            version = package.dependencies[name]
            packages.push(`${name}@${version}`)
          }
        }

        if (packages.length) {
          return npm.install(packages, {
            cwd: site.repo
          })
        } else {
          return Promise.resolve()
        }
      }).then((res) => {
        resolve()
      }).catch((err) => {
        console.log(err)
      })
    })
  }
}