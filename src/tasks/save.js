const fs = require('fs')


/**
 This task saves app data
 **/

module.exports = function (config) {
  return function (appData) {
    return new Promise((resolve, reject) => {
      const raw = JSON.stringify(appData)
      fs.writeFileSync(config.DATA_FILE, raw, 'utf8')
      resolve(appData)
    })
  }
}