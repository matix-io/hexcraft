const GitManager = require('../helpers/git-manager')
const npm = require('npm-programmatic')
const fs = require('fs')


module.exports = function (config) {
  return function (site, message) {
    return new Promise((resolve, reject) => {
      const repo = GitManager.get(site)
      repo.ready.then(() => {
        return repo.push({ message: message })
      }).then(() => {
        console.log('success')
        resolve()
      }).catch((err) => {
        console.log(err)
      })
    })
  }
}