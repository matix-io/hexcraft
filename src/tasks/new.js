const crypto = require('crypto')
const path = require('path')

const Site = require('../helpers/site')
const Git = require('../helpers/git')
const git = new Git()


/**
 This task makes a new site
 **/

module.exports = function (config) {
  return function (data) {
    return new Promise((resolve, reject) => {
      const randomHex = crypto.randomBytes(16).toString('hex');
      const repoPath = path.join(config.REPO_DIR, randomHex)

      git.clone({
        uri: data.uri,
        username: data.username,
        password: data.password,
        path: repoPath
      }).then((repo) => {
        data.repo = repoPath
        let site = new Site(data)
        resolve(site)
      }).catch((err) => {

      })
    })
  }
}