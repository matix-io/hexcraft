const path = require('path')


module.exports = function (config) {
  return function (site) {
    return new Promise((resolve, reject) => {
      try {
        const Hexo = require(`${site.repo}/node_modules/hexo`)
        const hexo = new Hexo(site.repo, {})

        hexo.init().then(() => {
          const config = {
            port: 4000,
            log: false,
            ip: '0.0.0.0',
            compress: false,
            header: true
          }

          const server = hexo.call('server', config).then((res) => {
            // console.log(res)
          }).catch((err) => {
            console.log(err)
          })
          resolve(server)
        })
      } catch (err) {
        console.log(err)
      }
    })
  }
}