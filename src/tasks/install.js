const fs = require('fs')
const npm = require('npm-programmatic')


module.exports = function (config) {
  return function (site) {
    return new Promise((resolve, reject) => {
      let package = JSON.parse(fs.readFileSync(`${site.repo}/package.json`, 'utf8'))
      let name
      let version
      let packages = []
      let promise
      for (name in package.dependencies) {
        try {
          let pkgPath = `${site.repo}/node_modules/${name}/package.json`
          let pkg = JSON.parse(fs.readFileSync(pkgPath, 'utf8'))
          // TODO check versioning
        } catch (err) {
          version = package.dependencies[name]
          packages.push(`${name}@${version}`)
        }
      }

      if (packages.length) {
        console.log('installing..')
        promise = npm.install(packages, {
          cwd: site.repo
        })
      } else {
        promise = Promise.resolve()
      }

      promise.then(() => {
          console.log('done installing')
          resolve()
        })
        .catch((err) => {
          console.log(err)
          reject(err)
        })
    })
  }
}