const { BrowserWindow, ipcMain } = require('electron')
const url = require('url')
const path = require('path')

let win

let open = function (data) {
  win = new BrowserWindow({
    width: 300,
    height: 500,
    minWidth: 300,
    minHeight: 500,
    titleBarStyle: 'hidden-inset'
  })

  win.loadURL(url.format({
    pathname: path.join(__dirname, 'index.html'),
    protocol: 'file:',
    slashes: true
  }))

  win.webContents.on('did-finish-load', () => {
    win.webContents.send('data', data)
  })
}

open.update = function (data) {
  win.webContents.send('data', data)
}

open.close = function () {
  win.close()
  win = null
}

module.exports = function (config, tasks) {
  return open
}