const { BrowserWindow } = require('electron')
const url = require('url')
const path = require('path')

let win

let open = function (args) {
  if (win) {
    return win
  } else {
    win = new BrowserWindow({
      width: 800,
      height: 500,
    })

    win.loadURL(args.uri)

    win.on('close', () => {
      win = null
    })
  }
}

open.close = function () {
  win.close()
  win = null
}

module.exports = function (config, tasks) {
  return open
}