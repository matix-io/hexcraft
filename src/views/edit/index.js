const { BrowserWindow } = require('electron')
const url = require('url')
const path = require('path')

let win

let open = function (args) {
  if (win) {
    return win
  } else {
    win = new BrowserWindow({
      width: 300,
      height: 205,
      minWidth: 300,
      minHeight: 205,
      titleBarStyle: 'hidden-inset'
    })

    win.loadURL(url.format({
      pathname: path.join(__dirname, 'index.html'),
      protocol: 'file:',
      slashes: true
    }))

    win.on('close', () => {
      win = null
    })
  }
}

open.close = function () {
  win.close()
  win = null
}

module.exports = function (config, tasks) {
  return open
}