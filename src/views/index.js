module.exports = function (config, tasks) {
  return {
    edit: require('./edit')(config, tasks),
    list: require('./list')(config, tasks),
    main: require('./main')(config, tasks),
    browser: require('./browser')(config, tasks),
  }
}