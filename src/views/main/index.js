const { BrowserWindow, ipcMain } = require('electron')
const url = require('url')
const path = require('path')

module.exports = function (config, tasks) {
  let win
  let open = function (data) {

    win = new BrowserWindow({
      width: 701,
      height: 500,
      minWidth: 701,
      minHeight: 500,
      titleBarStyle: 'hidden-inset'
    })

    win.loadURL(url.format({
      pathname: path.join(__dirname, 'index.html'),
      protocol: 'file:',
      slashes: true
    }))

    win.webContents.on('did-finish-load', () => {
      win.webContents.send('data', data)
    })

    tasks.launchServer(data.site).then(() => {
      tasks.isClean(data.site).then((isClean) => {
        win.webContents.send('loaded', {
          isClean
        })
      })
    })
  }

  open.update = function (data) {
    win.webContents.send('data', data)
  }

  return open
}