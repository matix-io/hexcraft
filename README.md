# Installation

1. Clone
2. Run `npm install` from the root
3. `cd src` then run `npm install` (two different packages because I'm still figuring out how the `electron-packager` command chooses which node modules to include in the build).
4. From the `src` directory, run `npm run fix-nodegit` to rebuild nodegit for the proper electron version
5. Run `npm i -g electron` to install the electron CLI


# Running

From the root run `electron src`


# Storage

All files are stored in `~/.developers-against-wordpress` on OSX.  This includes cloned repositories & application data.


# Building

`npm run build:osx` will generate an OSX executable in the `dist` directory.  No Windows support yet.